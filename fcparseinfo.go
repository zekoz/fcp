package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"

	log "github.com/golang/glog"
)

var (
	players = make(map[string]float64)
	extras = make(map[string]float64)
	terrains = make(map[string]float64)
	mp = make(map[int]bool)
	cid = 1000
)

func uninfo(info map[string]interface{}) ([]map[string]interface{}, error) {
	m, ok := info["message"].(string)
	if !ok {
		log.Exit("not string")
	}

	out := map[string]interface{}{
		"pid": 15,
		"known": 1,
		"extras_owner": 255,
		"owner": 255,
		"spec_sprite": "",
		"label": "",
		"resource": 128,
		"extras": []int{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
		"worked": 0,
	}

	var xtras []int
	var x, y int
	sp := strings.Split(m, "\n")
	for _, s := range sp {
		switch ss := strings.SplitN(s, " ", 2); ss[0] {
		case "Location:":
			var cont int
			if _, err := fmt.Sscanf(s, "Location: (%d, %d) [%d]", &x, &y, &cont); err != nil {
				log.Exit(err)
			}
			out["tile"] = y * 240 + x
			out["continent"] = cont
		case "Terrain:":
			sss := strings.Split(ss[1], " ")
			if sss[0] == "Deep" {
				out["terrain"] = terrains["Deep Ocean"]
			} else {
				tsp := strings.Split(sss[0], "/")
				tn, ok := terrains[tsp[0]]
				if !ok {
					log.Exitf("fail %s", s)
				}
				out["terrain"] = tn
			}
			if strings.HasSuffix(sss[len(sss)-1], ")") {
				res, ok := extras[strings.TrimPrefix(strings.TrimSuffix(sss[len(sss)-1], ")"), "(")]
				if !ok {
					log.Exitf("fail %s", s)
				}
				out["resource"] = res
				xtras = append(xtras, int(res))
			}
			if strings.Contains(sss[0], "/") {
				xtras = append(xtras, int(extras["River"]))
			}
		case "Territory":
			sss := strings.Split(ss[1], " ")
			var pname string
			if sss[1] == "AI" {
				pname = sss[2]
			} else {
				pname = sss[1]
			}

			pno, ok := players[strings.ToLower(pname)]
			if !ok {
				log.Exitf("fail %s", s)
			}
			out["owner"] = pno
		case "Infrastructure:":
			rd := strings.Contains(ss[1], "Road")
			rv := strings.Contains(ss[1], "River")
			if rd && rv {
				log.Infof("Bridge: %d %d", x, y)
			}
			for _, x := range []string{"Road", "Mine", "River", "Irrigation"} {
				if strings.Contains(ss[1], x) {
					xtras = append(xtras, int(extras[x]))
				}
			}
		}
	}

	for _, x := range xtras {
		out["extras"].([]int)[x/8] |= 1 << uint(x % 8)
	}

	l := []map[string]interface{}{out}

	if strings.Contains(m, "City:") {
		sss := strings.Split(m, "City: ")
		sss = strings.Split(sss[1], "|")
		var p, c string
		if _, err := fmt.Sscanf(sss[0], "%s ", &c); err != nil {
			log.Exit(err)
		}
		if _, err := fmt.Sscanf(sss[1], " %s ", &p); err != nil {
			log.Exit(err)
		}
		if len(sss) != 3 {
			log.Exitf("wut %q", m)
		}
		o := !strings.Contains(sss[2], "Not")
		cid++
		pno, ok := players[strings.ToLower(p)]
		if !ok {
			log.Exitf("fail %s", m)
		}
		l = append(l, map[string]interface{}{
			"pid": 32,
			"tile": y * 240 + x,
			"id": cid,
			"owner": pno,
			"size": 1,
			"style": 1,
			"occupied": o,
			"walls": false,
			"happy": true,
			"unhappy": false,
			"city_image": 1,
			"name": c,
			"improvements": []int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		})
	}

	return l, nil
}

func xform(r io.Reader, w io.Writer) {
	dec := json.NewDecoder(r)
	var out []map[string]interface{}

	for {
		// read open bracket
		_, err := dec.Token()
		if err != nil {
			log.Error(err)
			break
		}
		for dec.More() {
			var p map[string]interface{}
			err := dec.Decode(&p)
			if err != nil {
				log.Error(err)
				return
			}
			switch pid := p["pid"].(float64); int(pid) {
			// player_info
			case 51:
				pno, ok := p["playerno"].(float64)
				if !ok {
					log.Exit("err: %v", p)
				}
				nm, ok := p["username"].(string)
				if !ok {
					log.Exit("err: %v", p)
				}
				players[strings.ToLower(nm)] = pno
			// ruleset_terrain
			case 151:
				id, ok := p["id"].(float64)
				if !ok {
					log.Exit("err: %v", p)
				}
				nm, ok := p["rule_name"].(string)
				if !ok {
					log.Exit("err: %v", p)
				}
				terrains[nm] = id
			// ruleset_extra
			case 232:
				id, ok := p["id"].(float64)
				if !ok {
					log.Exit("err: %v", p)
				}
				nm, ok := p["rule_name"].(string)
				if !ok {
					log.Exit("err: %v", p)
				}
				extras[nm] = id
			// info
			case 290:
				ps, err := uninfo(p)
				if err != nil {
					log.Exit(err)
				}
				out = append(out, ps...)
			}
			out = append(out, p)
		}
		// read closing bracket
		_, err = dec.Token()
		if err != nil {
			log.Error(err)
			return
		}
	}
	enc := json.NewEncoder(w)
	enc.Encode(out)
}

func main() {
	flag.Parse()

	in, err := os.Open("dump.json")
	if err != nil {
		log.Exit(err)
	}
	out, err := os.Create("xform.json")
	if err != nil {
		log.Exit(err)
	}
	xform(in, out)
}
